# Configuracion 1 base de datos

1. Crear base de datos con el nombre "acre" en postgres con dbeaver
2. ejecutar el script acre.sql en la base de datos acre

# Configuracion 2 frontend
1. cd frontend
2. npm i 
3. npm run server(requerimiento node v18-20)

# Configuracion 3 backend
1. cd backend
2. composer install
3. php yii serve

# Postman

1. La api rest funciona en [http://localhost:8080/api/v1](http://localhost:8080/api/v1)
2. Se puede importar el archivo [acre.postman_collection.json](/path/to/acre.postman_collection.json) en postman para probar los endpoints
3. Ejemplo con facultad:
  - `http://localhost:8080/api/v1/facultad` -> get obtener todas las facultades
  - `http://localhost:8080/api/v1/facultad/view?id=2` -> get obtener facultad por id
  - `http://localhost:8080/api/v1/facultad/create` -> post crear facultad con(body>raw>json): 
  `{
    "Nombre": "Humanidades",
    "Sigla": "hum123"
   }`
  - `http://localhost:8080/api/v1/facultad/update?id=2` -> put actualizar facultad con(body>raw>json): 
  `{
    "Nombre": "Humanidades",
    "Sigla": "hum123"
   }`
  - `http://localhost:8080/api/v1/facultad/delete?id=2` -> delete eliminar facultad por id


## diagrama

![diagrama](https://www.plantuml.com/plantuml/svg/jLVRZjCm47tlLnXuMIIGk6WbwDE698iYRPigQH4IWifDHbjh4pkodOKLuj-nasskJfgMfNsfRC-PC-EPyUcLa9ZBF4rcp-GUKa1PWWcTIPOXoT2EISbI51CEaIICpiGTeHdcE4K9eI0VCa2CopsRpQ842u4iEt0TRstPdhz4lsS88Hjp3XnxpXmHAk4Mk5uDYP2GOYlY41E98uNTER4c6HlPTXNlUEp0DKHxx56YjkOenXAgnMj2wvNF86JVE8H7P6MxOom1JFLoyzIVnuXjAmZSm2g3NR9qns6ExZ5NZaeUECeJnRNZmMOnkMMj9IEwl_32AfSZuGuOh_obkroxfRa38k8aqzOjYWv9WOg9c86tTWFF7qUqMPen2ifs0gJZBTnb7UGGvW9un4JEfo3wzaPXbcMuxTVaM1vfJ29LMTnKJWwiE0WL5YxP7Oh8tmdWzyKYY1PfL-8Cdy3uI5vrKpWiojLzITQ1hTRTNn8uPHC32tA9smMh8mtp7Obn7U4evwtxLTN4qX_iWQ5m-hvpNJDzEW54aBLpRcBaXclByGqNFaJ02BAsLiFjRBAujkpDZV8yQ1IIsmGVXzaiDMEhwYu3afL1af2OYKwxgHdBFAf6cSnra3XfxsEkPyqH_vzL-ixAcBTqF5ibFZXpA5WPPpjLcaTdxS8DR3_S18Ff-G9SzARR2P6it43DsyVqhu2dHBJp5h1aGZS7_it9lJ20izs4cxeYbFkMqKRac1CsWLBeXMjtOHbwOsAHXvjFxST8NISnF1hz35tdjAZ9QKJNtkfVExY0w4nKFJcF6hPoFRdxJdoLKtBBiTOSfHkSPSd3Lgedk6LvtwdaYxSVthoiC7BrtfJc-XV5QStpujtbvSi2E9VxxHqyr3lltvKRKGH2R2Mx0zhRp1JyJyRZxHwBVH_qiDrA02qorKuAYPkHpreNslcy_lRYygF-lYgqv_VNFr2bgWoVENfUQqhFUNuojkcLgB6Dml8SDuTnQWwXrftdO1kxkS8sY-jp_FIRiHc3GQ9h9oDtPtHIFP_6H3NqSm5-c44JmKSYLk0czhLedkIWLgyczarfNQ0VvFD4-YDiT1yqNrGDFqB625yzg4Ro3SLTW0-hwgT9_QaEIgM61Zhi8BtFGQuLZGcvAQMRwFx4hFU5Swja-Zh-72V_DuJE4zj1x_vDqHv6-3n5vH-KszJ6QaghFdmfOZJko3aZxj3P4hVMQngxAQacfcHezfKf8Or9Sofuyz-7OGgtDDmv-5eJey5chHC-ai2dcJ9NGECyJVu2 "diagrama")
