import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="d-flex flex-column vh-100">
      <nav
        className="navbar navbar-expand-lg px-5"
        style={{ backgroundColor: "rgb(16, 38, 68)" }}
      >
        <div className="container-fluid">
          <div className="d-flex align-items-center">
            <a
              className="navbar-brand ms-3"
              href="/"
              style={{ color: "white" }}
            >
              <img
                src="/sisca.png"
                alt="Logo"
                className="navbar-brand-logo"
                width={70}
              />
              SISCA
            </a>
          </div>
          <div
            className="d-flex"
            style={{ backgroundColor: "#ab120a", borderRadius: "10px" }}
          >
            <Link to="/login" className="btn text-white">
              <strong>INICIO DE SESION</strong>
            </Link>
          </div>
        </div>
      </nav>
      <section className="flex-grow-1 d-flex">
        <div
          className="flex-grow-1 d-flex align-items-center justify-content-center flex-column"
          style={{ width: "40%", backgroundColor: "#ab120a" }}
        >
          <h1 className="text-white" style={{ fontSize: "3.5rem" }}>
            <strong>BIENVENIDOS AL</strong>
          </h1>
          <h2 className="text-white" style={{ fontSize: "3rem" }}>
            <em>Sistema de acreditacion</em>
          </h2>
        </div>
        <div className="flex-grow-1 d-flex align-items-center justify-content-center">
          <img src="/ode.png" alt="Fondo" />
        </div>
      </section>
      <footer
        className="text-white text-center py-3"
        style={{ backgroundColor: "rgb(16, 38, 68)" }}
      >
        DERECHOS RESERVADOS @ 2023 | DTIC UMSS
      </footer>
    </div>
  );
};

export default Home;
