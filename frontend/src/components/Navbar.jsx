import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Navegacion from "./Navegacion";
import { Dropdown } from "react-bootstrap";
import { FaUser } from "react-icons/fa";

const Navbar = () => {
  const [username, setUsername] = useState("");
  const accessToken = localStorage.getItem("accessToken"); // Obtén el access_token del local storage
  const navigate = useNavigate();

  if (!localStorage.getItem("accessToken")) {
    navigate("/");
  }

  useEffect(() => {
    const fetchUser = async () => {
      const response = await fetch(
        `http://localhost:8080/v1/user/get-user?access_token=${accessToken}`
      );
      if (response.ok) {
        const data = await response.json();
        setUsername(data.username);
      }
    };

    fetchUser();
  }, [accessToken]);

  const handleLogout = () => {
    localStorage.removeItem("accessToken"); // Elimina el access_token del local storage
    navigate("/");
  };

  return (
    <>
      <nav
        className="navbar navbar-expand-lg px-5"
        style={{ backgroundColor: "rgb(16, 38, 68)" }}
      >
        <div className="container-fluid">
          <div className="d-flex align-items-center">
            <img
              src="/sisca.png"
              alt="Logo"
              className="navbar-brand-logo"
              width={70}
            />
            <a
              className="navbar-brand ms-3"
              href="#"
              style={{ color: "white" }}
            >
              SISCA
            </a>
          </div>
          <Dropdown>
            <Dropdown.Toggle variant="danger" id="dropdown-basic">
              <FaUser size={30}/> {username}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item href="#/configuracion">
                Configuración
              </Dropdown.Item>
              <Dropdown.Item onClick={handleLogout}>
                Cerrar sesión
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </nav>
      <Navegacion />
    </>
  );
};

export default Navbar;
