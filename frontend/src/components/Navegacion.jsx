import { Navbar, Nav, Button } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import { useState, useEffect } from "react";

const Navegacion = () => {
  const location = useLocation();
  const [activeLink, setActiveLink] = useState("");

  useEffect(() => {
    setActiveLink(location.pathname);
  }, [location]);

  return (
    <Navbar
      expand="lg"
      style={{ backgroundColor: "rgb(16, 38, 68)" }}
      className="px-5"
    >
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <div
            className="d-flex"
            style={{ backgroundColor: "red", borderRadius: "200px" }}
          >
            <Button
              as={Link}
              to="/facultades"
              className={`btn ${
                activeLink === "/facultades"
                  ? "btn-light text-danger"
                  : "btn-danger text-white"
              }`}
            >
              Facultades
            </Button>
            <Button
              as={Link}
              to="/carreras"
              className={`btn ${
                activeLink === "/carreras"
                  ? "btn-light text-danger"
                  : "btn-danger text-white"
              }`}
            >
              Carreras
            </Button>
            {/* Agrega más enlaces aquí para otros tipos de datos */}
          </div>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Navegacion;
