import { useEffect, useState } from "react";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./Navbar";

function Carreras() {
  const [carreras, setCarreras] = useState([]);
  const [facultades, setFacultades] = useState([]);
  const [editingId, setEditingId] = useState(null);
  const [formData, setFormData] = useState({
    nombre: "",
    facultadID: 0,
    codigo: 0,
  });
  const [showModal, setShowModal] = useState(false);

  const [showCreateModal, setShowCreateModal] = useState(false);
  const [newCarrera, setNewCarrera] = useState({});

  useEffect(() => {
    const accessToken = localStorage.getItem("accessToken");

    axios
      .get("http://localhost:8080/v1/carrera", {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((response) => {
        const carrerasOrdenadas = response.data.sort((a, b) => a.ID - b.ID);
        setCarreras(carrerasOrdenadas);
      })
      .catch((error) => console.error(`Error: ${error}`));

    axios
      .get("http://localhost:8080/v1/facultad", {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((response) => {
        const facultadesOrdenadas = response.data.sort((a, b) => a.ID - b.ID);
        setFacultades(facultadesOrdenadas);
      })
      .catch((error) => console.error(`Error: ${error}`));
  }, []);

  const handleEdit = (id, nombre, codigo, facultadID) => {
    setEditingId(id);
    setFormData({ nombre, codigo, facultadID });
    setShowModal(true);
  };

  const handleDelete = (id) => {
    const accessToken = localStorage.getItem("accessToken");
    axios
      .delete(`http://localhost:8080/v1/carrera/delete?id=${id}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(() => setCarreras(carreras.filter((carrera) => carrera.ID !== id)))
      .catch((error) => console.error(`Error: ${error}`));
  };

  const handleFormChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
      [event.target.codigo]: event.target.value,
      [event.target.facultadID]: event.target.value,
    });
  };

  const handleFormSubmit = (event) => {
    console.log("llegue");
    event.preventDefault();
    const accessToken = localStorage.getItem("accessToken");
    console.log(formData, editingId);
    axios
      .put(
        `http://localhost:8080/v1/carrera/update?id=${editingId}`,
        {
          Nombre: formData.nombre,
          Codigo: formData.codigo,
          FacultadID: formData.facultadID,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then(() => {
        setCarreras(
          carreras.map((carrera) =>
            carrera.ID === editingId
              ? {
                  ...carrera,
                  Nombre: formData.nombre,
                  Codigo: formData.codigo,
                  FacultadID: formData.facultadID,
                }
              : carrera
          )
        );
        setShowModal(false);
        window.location.reload();
      })
      .catch((error) => console.error(`Error: ${error}`));
  };

  const handleCreate = () => {
    const accessToken = localStorage.getItem("accessToken");
    console.log(newCarrera);
    axios
      .post(
        `http://localhost:8080/v1/carrera/create`,
        {
          Nombre: newCarrera.nombre,
          Codigo: newCarrera.codigo,
          FacultadID: newCarrera.facultadID,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then((response) => {
        // console.log(response);
        setCarreras([...carreras, response.data]);
        setShowCreateModal(false);
        window.location.reload();
      })
      .catch((error) => console.error(`Error: ${error}`));
  };

  return (
    <>
      <Navbar />
      <div className="container">
        <h1 className="my-4">Carreras</h1>
        <Button
          variant="danger"
          onClick={() => setShowCreateModal(true)}
          className="mb-4"
        >
          Crear carrera
        </Button>
        <Modal
          show={showCreateModal}
          onHide={() => setShowCreateModal(false)}
          dialogClassName="modal-dialog-centered"
        >
          <Modal.Header closeButton>
            <Modal.Title>Crear carrera</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  value={newCarrera.nombre}
                  onChange={(e) =>
                    setNewCarrera({ ...newCarrera, nombre: e.target.value })
                  }
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Codigo</Form.Label>
                <Form.Control
                  type="number"
                  value={newCarrera.codigo}
                  onChange={(e) =>
                    setNewCarrera({ ...newCarrera, codigo: e.target.value })
                  }
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Facultad</Form.Label>
                <Form.Control
                  as="select"
                  value={newCarrera.facultadID}
                  onChange={(e) =>
                    setNewCarrera({
                      ...newCarrera,
                      facultadID: e.target.value,
                    })
                  }
                >
                  <option value="">Seleccione una facultad</option>
                  {facultades.map((facultad) => (
                    <option key={facultad.ID} value={facultad.ID}>
                      {facultad.Nombre}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={() => setShowCreateModal(false)}
            >
              Cerrar
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Guardar
            </Button>
          </Modal.Footer>
        </Modal>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Codigo</th>
              <th>Facultad</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {carreras &&
              carreras.length > 0 &&
              carreras.map((carrera) => (
                <tr key={carrera.ID}>
                  <td>{carrera.ID}</td>
                  <td>{carrera.Nombre}</td>
                  <td>{carrera.Codigo}</td>
                  <td>
                    {
                      facultades.find(
                        (facultad) => facultad.ID === carrera.FacultadID
                      )?.Nombre
                    }
                  </td>
                  <td>
                    <button
                      className="btn btn-primary me-3"
                      onClick={() =>
                        handleEdit(
                          carrera.ID,
                          carrera.Nombre,
                          carrera.Codigo,
                          carrera.FacultadID
                        )
                      }
                    >
                      Editar
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDelete(carrera.ID)}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>

        <Modal
          show={showModal}
          onHide={() => setShowModal(false)}
          dialogClassName="modal-dialog-centered"
        >
          <Modal.Header closeButton>
            <Modal.Title>Editar Carrera</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleFormSubmit}>
              <Form.Group controlId="formNombre">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  name="nombre"
                  value={formData.nombre}
                  onChange={handleFormChange}
                />
              </Form.Group>
              <Form.Group className="my-3">
                <Form.Label>Codigo</Form.Label>
                <Form.Control
                  type="number"
                  name="codigo"
                  value={formData.codigo}
                  onChange={handleFormChange}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Facultad</Form.Label>
                <Form.Control
                  as="select"
                  name="facultadID"
                  value={formData.facultadID}
                  onChange={handleFormChange}
                >
                  {facultades.map((facultad) => (
                    <option key={facultad.ID} value={facultad.ID}>
                      {facultad.Nombre}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
              <Modal.Footer>
                <Button variant="secondary" onClick={() => setShowModal(false)}>
                  Cerrar
                </Button>
                <Button variant="primary" type="submit">
                  Guardar
                </Button>
              </Modal.Footer>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
}
export default Carreras;
