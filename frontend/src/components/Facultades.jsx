import { useEffect, useState } from "react";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./Navbar";

function Facultades() {
  const [facultades, setFacultades] = useState([]);
  const [editingId, setEditingId] = useState(null);
  const [formData, setFormData] = useState({ Nombre: "", Sigla: "" });
  const [showModal, setShowModal] = useState(false);

  const [showCreateModal, setShowCreateModal] = useState(false);
  const [newFacultad, setNewFacultad] = useState({ Nombre: "", Sigla: "" });

  useEffect(() => {
    const accessToken = localStorage.getItem("accessToken");
    console.log(accessToken);

    axios
      .get("http://localhost:8080/v1/facultad", {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((response) => {
        const facultadesOrdenadas = response.data.sort((a, b) => a.ID - b.ID);
        setFacultades(facultadesOrdenadas);
      })
      .catch((error) => console.error(`Error: ${error}`));
  }, []);

  const handleEdit = (id, nombre, sigla) => {
    setEditingId(id);
    setFormData({ nombre, sigla });
    setShowModal(true);
  };

  const handleDelete = (id) => {
    const accessToken = localStorage.getItem("accessToken");
    axios
      .delete(`http://localhost:8080/v1/facultad/delete?id=${id}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then(() =>
        setFacultades(facultades.filter((facultad) => facultad.ID !== id))
      )
      .catch((error) => console.error(`Error: ${error}`));
  };

  const handleFormChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
      [event.target.sigla]: event.target.value,
    });
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    const accessToken = localStorage.getItem("accessToken");
    console.log(formData, editingId);
    axios
      .put(
        `http://localhost:8080/v1/facultad/update?id=${editingId}`,
        {
          Nombre: formData.nombre,
          Sigla: formData.sigla,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then(() => {
        setFacultades(
          facultades.map((facultad) =>
            facultad.ID === editingId
              ? { ...facultad, Nombre: formData.nombre, Sigla: formData.sigla }
              : facultad
          )
        );
        setShowModal(false);
      })
      .catch((error) => console.error(`Error: ${error}`));
  };

  const handleCreate = () => {
    const accessToken = localStorage.getItem("accessToken");
    axios
      .post(
        `http://localhost:8080/v1/facultad/create`,
        {
          Nombre: newFacultad.Nombre,
          Sigla: newFacultad.Sigla,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then((response) => {
        // console.log(response);
        setFacultades([...facultades, response.data]);
        setShowCreateModal(false);
      })
      .catch((error) => console.error(`Error: ${error}`));
  };

  return (
    <>
      <Navbar />
      <div className="container">
        <h1 className="my-4">Facultades</h1>
        <Button
          variant="danger"
          onClick={() => setShowCreateModal(true)}
          className="mb-4"
        >
          Crear facultad
        </Button>
        <Modal
          show={showCreateModal}
          onHide={() => setShowCreateModal(false)}
          dialogClassName="modal-dialog-centered"
        >
          <Modal.Header closeButton>
            <Modal.Title>Crear facultad</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  value={newFacultad.nombre}
                  onChange={(e) =>
                    setNewFacultad({ ...newFacultad, Nombre: e.target.value })
                  }
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Sigla</Form.Label>
                <Form.Control
                  type="text"
                  value={newFacultad.sigla}
                  onChange={(e) =>
                    setNewFacultad({ ...newFacultad, Sigla: e.target.value })
                  }
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={() => setShowCreateModal(false)}
            >
              Cerrar
            </Button>
            <Button variant="primary" onClick={handleCreate}>
              Guardar
            </Button>
          </Modal.Footer>
        </Modal>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Sigla</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            {facultades &&
              facultades.length > 0 &&
              facultades.map((facultad) => (
                <tr key={facultad.ID}>
                  <td>{facultad.ID}</td>
                  <td>{facultad.Nombre}</td>
                  <td>{facultad.Sigla}</td>
                  <td>
                    <button
                      className="btn btn-primary me-3"
                      onClick={() =>
                        handleEdit(facultad.ID, facultad.Nombre, facultad.Sigla)
                      }
                    >
                      Editar
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDelete(facultad.ID)}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>

        <Modal
          show={showModal}
          onHide={() => setShowModal(false)}
          dialogClassName="modal-dialog-centered"
        >
          <Modal.Header closeButton>
            <Modal.Title>Editar Facultad</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleFormSubmit}>
              <Form.Group controlId="formNombre">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  name="nombre"
                  value={formData.nombre}
                  onChange={handleFormChange}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Sigla</Form.Label>
                <Form.Control
                  type="text"
                  name="sigla"
                  value={formData.sigla}
                  onChange={handleFormChange}
                />
              </Form.Group>
              <Button variant="primary" type="submit" className="mt-2">
                Guardar
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
}
export default Facultades;
