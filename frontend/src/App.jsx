import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './components/Login';
import Facultades from './components/Facultades';
import Home from './components/Home';
import Carreras from './components/Carreras';


function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/facultades" element={<Facultades />} />
          <Route path="/carreras" element={<Carreras />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </Router>
    </>
  );
}
export default App;