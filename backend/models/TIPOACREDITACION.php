<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TIPO_ACREDITACION".
 *
 * @property int $ID
 * @property string $Descripcion
 *
 * @property Acreditacion[] $acreditacions
 */
class TIPOACREDITACION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TIPO_ACREDITACION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Descripcion'], 'required'],
            [['Descripcion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Acreditacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacions()
    {
        return $this->hasMany(ACREDITACION::class, ['TipoAcreditacionID' => 'ID']);
    }
}
