<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SISTEMA_ACREDITACION".
 *
 * @property int $ID
 * @property string $Nombre
 *
 * @property Acreditacion[] $acreditacions
 */
class SISTEMAACREDITACION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'SISTEMA_ACREDITACION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Acreditacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacions()
    {
        return $this->hasMany(ACREDITACION::class, ['SistemaAcreditacionID' => 'ID']);
    }
}
