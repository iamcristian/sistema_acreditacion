<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "COMISION".
 *
 * @property int $ID
 * @property int $AcreditacionID
 *
 * @property Acreditacion $acreditacion
 * @property Subcomision[] $subcomisions
 */
class COMISION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'COMISION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AcreditacionID'], 'required'],
            [['AcreditacionID'], 'default', 'value' => null],
            [['AcreditacionID'], 'integer'],
            [['AcreditacionID'], 'exist', 'skipOnError' => true, 'targetClass' => ACREDITACION::class, 'targetAttribute' => ['AcreditacionID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AcreditacionID' => 'Acreditacion ID',
        ];
    }

    /**
     * Gets query for [[Acreditacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacion()
    {
        return $this->hasOne(ACREDITACION::class, ['ID' => 'AcreditacionID']);
    }

    /**
     * Gets query for [[Subcomisions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubcomisions()
    {
        return $this->hasMany(SUBCOMISION::class, ['ComisionID' => 'ID']);
    }
}
