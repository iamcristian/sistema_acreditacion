<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CARRERA".
 *
 * @property int $ID
 * @property string $Nombre
 * @property int $FacultadID
 * @property int $Codigo
 *
 * @property Acreditacion[] $acreditacions
 * @property Facultad $facultad
 */
class CARRERA extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CARRERA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'FacultadID', 'Codigo'], 'required'],
            [['Nombre'], 'string'],
            [['FacultadID', 'Codigo'], 'default', 'value' => null],
            [['FacultadID', 'Codigo'], 'integer'],
            [['FacultadID'], 'exist', 'skipOnError' => true, 'targetClass' => FACULTAD::class, 'targetAttribute' => ['FacultadID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'FacultadID' => 'Facultad ID',
            'Codigo' => 'Codigo',
        ];
    }

    /**
     * Gets query for [[Acreditacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacions()
    {
        return $this->hasMany(ACREDITACION::class, ['CarreraID' => 'ID']);
    }

    /**
     * Gets query for [[Facultad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFacultad()
    {
        return $this->hasOne(FACULTAD::class, ['ID' => 'FacultadID']);
    }
}
