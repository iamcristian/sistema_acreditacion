<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "FACULTAD".
 *
 * @property int $ID
 * @property string $Nombre
 * @property string $Sigla
 *
 * @property Carrera[] $carreras
 */
class FACULTAD extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'FACULTAD';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Sigla'], 'required'],
            [['Nombre', 'Sigla'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Sigla' => 'Sigla',
        ];
    }

    /**
     * Gets query for [[Carreras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarreras()
    {
        return $this->hasMany(CARRERA::class, ['FacultadID' => 'ID']);
    }
}
