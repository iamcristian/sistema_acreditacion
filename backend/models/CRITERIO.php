<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CRITERIO".
 *
 * @property int $ID
 * @property string $Descripcion
 * @property int $ComponenteID
 *
 * @property Componente $componente
 */
class CRITERIO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CRITERIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Descripcion', 'ComponenteID'], 'required'],
            [['Descripcion'], 'string'],
            [['ComponenteID'], 'default', 'value' => null],
            [['ComponenteID'], 'integer'],
            [['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => COMPONENTE::class, 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'ComponenteID' => 'Componente ID',
        ];
    }

    /**
     * Gets query for [[Componente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(COMPONENTE::class, ['ID' => 'ComponenteID']);
    }
}
