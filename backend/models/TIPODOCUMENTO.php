<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TIPO_DOCUMENTO".
 *
 * @property int $ID
 * @property string $Tipo
 *
 * @property Documento[] $documentos
 */
class TIPODOCUMENTO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'TIPO_DOCUMENTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Tipo'], 'required'],
            [['Tipo'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(DOCUMENTO::class, ['TipoDocumentoID' => 'ID']);
    }
}
