<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "USUARIO".
 *
 * @property int $ID
 * @property string|null $Nombre
 * @property string|null $Apellidos
 * @property int|null $AcreditacionID
 *
 * @property Acreditacion $acreditacion
 * @property RolUsuario[] $rolUsuarios
 */
class USUARIO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'USUARIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellidos'], 'string'],
            [['AcreditacionID'], 'default', 'value' => null],
            [['AcreditacionID'], 'integer'],
            [['AcreditacionID'], 'exist', 'skipOnError' => true, 'targetClass' => ACREDITACION::class, 'targetAttribute' => ['AcreditacionID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'AcreditacionID' => 'Acreditacion ID',
        ];
    }

    /**
     * Gets query for [[Acreditacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacion()
    {
        return $this->hasOne(ACREDITACION::class, ['ID' => 'AcreditacionID']);
    }

    /**
     * Gets query for [[RolUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRolUsuarios()
    {
        return $this->hasMany(ROLUSUARIO::class, ['UsuarioID' => 'ID']);
    }
}
