<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MERCOSUR".
 *
 * @property int $ID
 * @property string $Nombre
 * @property string $Version
 * @property string|null $Descripcion
 *
 * @property Dimension[] $dimensions
 */
class MERCOSUR extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'MERCOSUR';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Version'], 'required'],
            [['Nombre', 'Version', 'Descripcion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Version' => 'Version',
            'Descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Dimensions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDimensions()
    {
        return $this->hasMany(DIMENSION::class, ['MercosurID' => 'ID']);
    }
}
