<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "FUNCIONARIO".
 *
 * @property int $ID
 * @property string $Nombre
 * @property string $Apellidos
 * @property string $CorreoInstitucional
 * @property string|null $Cargo
 *
 * @property Subcomision[] $subcomisions
 */
class FUNCIONARIO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'FUNCIONARIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellidos', 'CorreoInstitucional'], 'required'],
            [['Nombre', 'Apellidos', 'CorreoInstitucional', 'Cargo'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'CorreoInstitucional' => 'Correo Institucional',
            'Cargo' => 'Cargo',
        ];
    }

    /**
     * Gets query for [[Subcomisions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubcomisions()
    {
        return $this->hasMany(SUBCOMISION::class, ['FuncionarioID' => 'ID']);
    }
}
