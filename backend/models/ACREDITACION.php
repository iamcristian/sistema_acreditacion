<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ACREDITACION".
 *
 * @property int $ID
 * @property int $CarreraID
 * @property int $SistemaAcreditacionID
 * @property int $TipoAcreditacionID
 * @property int $EstadoAcreditacionID
 * @property string|null $FechaInicio
 * @property string|null $FechaFin
 * @property int $GestionID
 * @property bool|null $SeAcredito
 *
 * @property Carrera $carrera
 * @property Comision[] $comisions
 * @property DocumentacionAcreditacion[] $documentacionAcreditacions
 * @property EstadoAcreditacion $estadoAcreditacion
 * @property Gestion $gestion
 * @property SistemaAcreditacion $sistemaAcreditacion
 * @property TipoAcreditacion $tipoAcreditacion
 * @property Usuario[] $usuarios
 */
class ACREDITACION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ACREDITACION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CarreraID', 'SistemaAcreditacionID', 'TipoAcreditacionID', 'EstadoAcreditacionID', 'GestionID'], 'required'],
            [['CarreraID', 'SistemaAcreditacionID', 'TipoAcreditacionID', 'EstadoAcreditacionID', 'GestionID'], 'default', 'value' => null],
            [['CarreraID', 'SistemaAcreditacionID', 'TipoAcreditacionID', 'EstadoAcreditacionID', 'GestionID'], 'integer'],
            [['FechaInicio', 'FechaFin'], 'safe'],
            [['SeAcredito'], 'boolean'],
            [['CarreraID'], 'exist', 'skipOnError' => true, 'targetClass' => CARRERA::class, 'targetAttribute' => ['CarreraID' => 'ID']],
            [['EstadoAcreditacionID'], 'exist', 'skipOnError' => true, 'targetClass' => ESTADOACREDITACION::class, 'targetAttribute' => ['EstadoAcreditacionID' => 'ID']],
            [['GestionID'], 'exist', 'skipOnError' => true, 'targetClass' => GESTION::class, 'targetAttribute' => ['GestionID' => 'ID']],
            [['SistemaAcreditacionID'], 'exist', 'skipOnError' => true, 'targetClass' => SISTEMAACREDITACION::class, 'targetAttribute' => ['SistemaAcreditacionID' => 'ID']],
            [['TipoAcreditacionID'], 'exist', 'skipOnError' => true, 'targetClass' => TIPOACREDITACION::class, 'targetAttribute' => ['TipoAcreditacionID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CarreraID' => 'Carrera ID',
            'SistemaAcreditacionID' => 'Sistema Acreditacion ID',
            'TipoAcreditacionID' => 'Tipo Acreditacion ID',
            'EstadoAcreditacionID' => 'Estado Acreditacion ID',
            'FechaInicio' => 'Fecha Inicio',
            'FechaFin' => 'Fecha Fin',
            'GestionID' => 'Gestion ID',
            'SeAcredito' => 'Se Acredito',
        ];
    }

    /**
     * Gets query for [[Carrera]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarrera()
    {
        return $this->hasOne(CARRERA::class, ['ID' => 'CarreraID']);
    }

    /**
     * Gets query for [[Comisions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComisions()
    {
        return $this->hasMany(COMISION::class, ['AcreditacionID' => 'ID']);
    }

    /**
     * Gets query for [[DocumentacionAcreditacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentacionAcreditacions()
    {
        return $this->hasMany(DOCUMENTACIONACREDITACION::class, ['AcreditacionID' => 'ID']);
    }

    /**
     * Gets query for [[EstadoAcreditacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoAcreditacion()
    {
        return $this->hasOne(ESTADOACREDITACION::class, ['ID' => 'EstadoAcreditacionID']);
    }

    /**
     * Gets query for [[Gestion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGestion()
    {
        return $this->hasOne(GESTION::class, ['ID' => 'GestionID']);
    }

    /**
     * Gets query for [[SistemaAcreditacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSistemaAcreditacion()
    {
        return $this->hasOne(SISTEMAACREDITACION::class, ['ID' => 'SistemaAcreditacionID']);
    }

    /**
     * Gets query for [[TipoAcreditacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoAcreditacion()
    {
        return $this->hasOne(TIPOACREDITACION::class, ['ID' => 'TipoAcreditacionID']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(USUARIO::class, ['AcreditacionID' => 'ID']);
    }
}
