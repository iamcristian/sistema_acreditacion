<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "COMPONENTE".
 *
 * @property int $ID
 * @property string $Descripcion
 * @property int $DimensionID
 *
 * @property Criterio[] $criterios
 * @property Dimension $dimension
 * @property Indicador[] $indicadors
 */
class COMPONENTE extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'COMPONENTE';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Descripcion', 'DimensionID'], 'required'],
            [['Descripcion'], 'string'],
            [['DimensionID'], 'default', 'value' => null],
            [['DimensionID'], 'integer'],
            [['DimensionID'], 'exist', 'skipOnError' => true, 'targetClass' => DIMENSION::class, 'targetAttribute' => ['DimensionID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'DimensionID' => 'Dimension ID',
        ];
    }

    /**
     * Gets query for [[Criterios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCriterios()
    {
        return $this->hasMany(CRITERIO::class, ['ComponenteID' => 'ID']);
    }

    /**
     * Gets query for [[Dimension]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDimension()
    {
        return $this->hasOne(DIMENSION::class, ['ID' => 'DimensionID']);
    }

    /**
     * Gets query for [[Indicadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndicadors()
    {
        return $this->hasMany(INDICADOR::class, ['ComponenteID' => 'ID']);
    }
}
