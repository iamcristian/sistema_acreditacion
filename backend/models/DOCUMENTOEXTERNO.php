<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DOCUMENTO_EXTERNO".
 *
 * @property int $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property string $Ruta
 * @property string $Fuente
 * @property string $FechaRecepcion
 *
 * @property Documento[] $documentos
 */
class DOCUMENTOEXTERNO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'DOCUMENTO_EXTERNO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Descripcion', 'Ruta', 'Fuente', 'FechaRecepcion'], 'required'],
            [['Nombre', 'Descripcion', 'Ruta', 'Fuente'], 'string'],
            [['FechaRecepcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Descripcion' => 'Descripcion',
            'Ruta' => 'Ruta',
            'Fuente' => 'Fuente',
            'FechaRecepcion' => 'Fecha Recepcion',
        ];
    }

    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(DOCUMENTO::class, ['DocumentoExternoID' => 'ID']);
    }
}
