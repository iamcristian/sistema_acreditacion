<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DOCUMENTO".
 *
 * @property int $ID
 * @property string $Descripcion
 * @property int $TipoDocumentoID
 * @property int $DocumentoExternoID
 * @property int $IndicadorID
 * @property string $Ruta
 * @property string $FechaSubida
 *
 * @property DocumentoExterno $documentoExterno
 * @property Indicador $indicador
 * @property TipoDocumento $tipoDocumento
 */
class DOCUMENTO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'DOCUMENTO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Descripcion', 'TipoDocumentoID', 'DocumentoExternoID', 'IndicadorID', 'Ruta', 'FechaSubida'], 'required'],
            [['Descripcion', 'Ruta'], 'string'],
            [['TipoDocumentoID', 'DocumentoExternoID', 'IndicadorID'], 'default', 'value' => null],
            [['TipoDocumentoID', 'DocumentoExternoID', 'IndicadorID'], 'integer'],
            [['FechaSubida'], 'safe'],
            [['DocumentoExternoID'], 'exist', 'skipOnError' => true, 'targetClass' => DOCUMENTOEXTERNO::class, 'targetAttribute' => ['DocumentoExternoID' => 'ID']],
            [['IndicadorID'], 'exist', 'skipOnError' => true, 'targetClass' => INDICADOR::class, 'targetAttribute' => ['IndicadorID' => 'ID']],
            [['TipoDocumentoID'], 'exist', 'skipOnError' => true, 'targetClass' => TIPODOCUMENTO::class, 'targetAttribute' => ['TipoDocumentoID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'TipoDocumentoID' => 'Tipo Documento ID',
            'DocumentoExternoID' => 'Documento Externo ID',
            'IndicadorID' => 'Indicador ID',
            'Ruta' => 'Ruta',
            'FechaSubida' => 'Fecha Subida',
        ];
    }

    /**
     * Gets query for [[DocumentoExterno]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentoExterno()
    {
        return $this->hasOne(DOCUMENTOEXTERNO::class, ['ID' => 'DocumentoExternoID']);
    }

    /**
     * Gets query for [[Indicador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndicador()
    {
        return $this->hasOne(INDICADOR::class, ['ID' => 'IndicadorID']);
    }

    /**
     * Gets query for [[TipoDocumento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TIPODOCUMENTO::class, ['ID' => 'TipoDocumentoID']);
    }
}
