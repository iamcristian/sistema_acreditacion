<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DIMENSION".
 *
 * @property int $ID
 * @property string $Descripcion
 * @property int $MercosurID
 *
 * @property Componente[] $componentes
 * @property Mercosur $mercosur
 */
class DIMENSION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'DIMENSION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Descripcion', 'MercosurID'], 'required'],
            [['Descripcion'], 'string'],
            [['MercosurID'], 'default', 'value' => null],
            [['MercosurID'], 'integer'],
            [['MercosurID'], 'exist', 'skipOnError' => true, 'targetClass' => MERCOSUR::class, 'targetAttribute' => ['MercosurID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'MercosurID' => 'Mercosur ID',
        ];
    }

    /**
     * Gets query for [[Componentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponentes()
    {
        return $this->hasMany(COMPONENTE::class, ['DimensionID' => 'ID']);
    }

    /**
     * Gets query for [[Mercosur]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMercosur()
    {
        return $this->hasOne(MERCOSUR::class, ['ID' => 'MercosurID']);
    }
}
