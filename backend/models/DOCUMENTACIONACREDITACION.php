<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DOCUMENTACION_ACREDITACION".
 *
 * @property int $ID
 * @property int $AcreditacionID
 * @property int|null $IndicadorID
 * @property string $FechaDePresentacion
 * @property string|null $Observaciones
 * @property int $EtapaID
 *
 * @property Acreditacion $acreditacion
 * @property Etapa $etapa
 * @property Indicador $indicador
 */
class DOCUMENTACIONACREDITACION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'DOCUMENTACION_ACREDITACION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AcreditacionID', 'FechaDePresentacion', 'EtapaID'], 'required'],
            [['AcreditacionID', 'IndicadorID', 'EtapaID'], 'default', 'value' => null],
            [['AcreditacionID', 'IndicadorID', 'EtapaID'], 'integer'],
            [['FechaDePresentacion'], 'safe'],
            [['Observaciones'], 'string'],
            [['AcreditacionID'], 'exist', 'skipOnError' => true, 'targetClass' => ACREDITACION::class, 'targetAttribute' => ['AcreditacionID' => 'ID']],
            [['EtapaID'], 'exist', 'skipOnError' => true, 'targetClass' => ETAPA::class, 'targetAttribute' => ['EtapaID' => 'ID']],
            [['IndicadorID'], 'exist', 'skipOnError' => true, 'targetClass' => INDICADOR::class, 'targetAttribute' => ['IndicadorID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AcreditacionID' => 'Acreditacion ID',
            'IndicadorID' => 'Indicador ID',
            'FechaDePresentacion' => 'Fecha De Presentacion',
            'Observaciones' => 'Observaciones',
            'EtapaID' => 'Etapa ID',
        ];
    }

    /**
     * Gets query for [[Acreditacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacion()
    {
        return $this->hasOne(ACREDITACION::class, ['ID' => 'AcreditacionID']);
    }

    /**
     * Gets query for [[Etapa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEtapa()
    {
        return $this->hasOne(ETAPA::class, ['ID' => 'EtapaID']);
    }

    /**
     * Gets query for [[Indicador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndicador()
    {
        return $this->hasOne(INDICADOR::class, ['ID' => 'IndicadorID']);
    }
}
