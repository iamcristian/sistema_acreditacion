<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "INDICADOR".
 *
 * @property int $ID
 * @property string $Descripcion
 * @property int $ComponenteID
 * @property bool $Aprobado
 *
 * @property Componente $componente
 * @property DocumentacionAcreditacion[] $documentacionAcreditacions
 * @property Documento[] $documentos
 */
class INDICADOR extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'INDICADOR';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Descripcion', 'ComponenteID', 'Aprobado'], 'required'],
            [['Descripcion'], 'string'],
            [['ComponenteID'], 'default', 'value' => null],
            [['ComponenteID'], 'integer'],
            [['Aprobado'], 'boolean'],
            [['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => COMPONENTE::class, 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'ComponenteID' => 'Componente ID',
            'Aprobado' => 'Aprobado',
        ];
    }

    /**
     * Gets query for [[Componente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(COMPONENTE::class, ['ID' => 'ComponenteID']);
    }

    /**
     * Gets query for [[DocumentacionAcreditacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentacionAcreditacions()
    {
        return $this->hasMany(DOCUMENTACIONACREDITACION::class, ['IndicadorID' => 'ID']);
    }

    /**
     * Gets query for [[Documentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(DOCUMENTO::class, ['IndicadorID' => 'ID']);
    }
}
