<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ROL_USUARIO".
 *
 * @property int $ID
 * @property string|null $Rol
 * @property int|null $UsuarioID
 *
 * @property Permiso[] $permisos
 * @property Usuario $usuario
 */
class ROLUSUARIO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ROL_USUARIO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Rol'], 'string'],
            [['UsuarioID'], 'default', 'value' => null],
            [['UsuarioID'], 'integer'],
            [['UsuarioID'], 'exist', 'skipOnError' => true, 'targetClass' => USUARIO::class, 'targetAttribute' => ['UsuarioID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Rol' => 'Rol',
            'UsuarioID' => 'Usuario ID',
        ];
    }

    /**
     * Gets query for [[Permisos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPermisos()
    {
        return $this->hasMany(PERMISO::class, ['RolID' => 'ID']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(USUARIO::class, ['ID' => 'UsuarioID']);
    }
}
