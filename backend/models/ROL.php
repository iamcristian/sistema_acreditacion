<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ROL".
 *
 * @property int $ID
 * @property string $Nombre
 * @property string|null $Descripcion
 *
 * @property Subcomision[] $subcomisions
 */
class ROL extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ROL';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre', 'Descripcion'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Subcomisions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubcomisions()
    {
        return $this->hasMany(SUBCOMISION::class, ['RolID' => 'ID']);
    }
}
