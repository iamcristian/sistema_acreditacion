<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SUBCOMISION".
 *
 * @property int $ID
 * @property int $ComisionID
 * @property int $RolID
 * @property int $FuncionarioID
 *
 * @property Comision $comision
 * @property Funcionario $funcionario
 * @property Rol $rol
 */
class SUBCOMISION extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'SUBCOMISION';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ComisionID', 'RolID', 'FuncionarioID'], 'required'],
            [['ComisionID', 'RolID', 'FuncionarioID'], 'default', 'value' => null],
            [['ComisionID', 'RolID', 'FuncionarioID'], 'integer'],
            [['ComisionID'], 'exist', 'skipOnError' => true, 'targetClass' => COMISION::class, 'targetAttribute' => ['ComisionID' => 'ID']],
            [['FuncionarioID'], 'exist', 'skipOnError' => true, 'targetClass' => FUNCIONARIO::class, 'targetAttribute' => ['FuncionarioID' => 'ID']],
            [['RolID'], 'exist', 'skipOnError' => true, 'targetClass' => ROL::class, 'targetAttribute' => ['RolID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComisionID' => 'Comision ID',
            'RolID' => 'Rol ID',
            'FuncionarioID' => 'Funcionario ID',
        ];
    }

    /**
     * Gets query for [[Comision]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComision()
    {
        return $this->hasOne(COMISION::class, ['ID' => 'ComisionID']);
    }

    /**
     * Gets query for [[Funcionario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionario()
    {
        return $this->hasOne(FUNCIONARIO::class, ['ID' => 'FuncionarioID']);
    }

    /**
     * Gets query for [[Rol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(ROL::class, ['ID' => 'RolID']);
    }
}
