<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ETAPA".
 *
 * @property int $ID
 * @property string $Nombre
 *
 * @property DocumentacionAcreditacion[] $documentacionAcreditacions
 */
class ETAPA extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ETAPA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[DocumentacionAcreditacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentacionAcreditacions()
    {
        return $this->hasMany(DOCUMENTACIONACREDITACION::class, ['EtapaID' => 'ID']);
    }
}
