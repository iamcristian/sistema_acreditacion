<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PERMISO".
 *
 * @property int $ID
 * @property string|null $Permiso
 * @property int|null $RolID
 *
 * @property RolUsuario $rol
 */
class PERMISO extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PERMISO';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Permiso'], 'string'],
            [['RolID'], 'default', 'value' => null],
            [['RolID'], 'integer'],
            [['RolID'], 'exist', 'skipOnError' => true, 'targetClass' => ROLUSUARIO::class, 'targetAttribute' => ['RolID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Permiso' => 'Permiso',
            'RolID' => 'Rol ID',
        ];
    }

    /**
     * Gets query for [[Rol]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(ROLUSUARIO::class, ['ID' => 'RolID']);
    }
}
