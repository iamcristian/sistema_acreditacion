<?php
namespace app\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\User;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    public function actionLogin()
{
    $request = Yii::$app->request;
    $username = $request->post('username');
    $password = $request->post('password');

    $user = User::findByUsername($username);
    if ($user && $user->validatePassword($password)) {
        // Las credenciales son válidas
        $user->generateAccessToken();
        if ($user->save()) {
            // El token de acceso se ha guardado correctamente
            return $this->asJson(['access_token' => $user->access_token]);
        }
    }

    // Las credenciales no son válidas o no se pudo guardar el token de acceso
    return $this->asJson(['error' => 'Invalid credentials or could not save access token']);
}

public function actionGetUser()
{
    $request = Yii::$app->request;
    $accessToken = $request->get('access_token');

    $user = User::findIdentityByAccessToken($accessToken);
    if ($user) {
        // El token de acceso es válido
        return $this->asJson([
            'username' => $user->username,
            // Agrega aquí cualquier otra información del usuario que quieras devolver
        ]);
    }

    // El token de acceso no es válido
    return $this->asJson(['error' => 'Invalid access token']);
}
}
?>