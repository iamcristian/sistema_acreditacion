<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\Cors;

/**
 * Default controller for the `v1` module
 */
class CarreraController extends ActiveController
{
    public $modelClass = "app\models\CARRERA";

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::class,
        ];
        return $behaviors;
    }

    public function beforeAction($action) 	
    {     	
    if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {         	
        Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT');         	
        Yii::$app->end();     	
    }        	     	
    return parent::beforeAction($action); 	
    }
}
