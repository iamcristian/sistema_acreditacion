<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\Cors;

/**
 * Default controller for the `v1` module
 */
class FacultadController extends ActiveController
{
    public $modelClass = "app\models\FACULTAD";
    /*
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::class,
        ];
        return $behaviors;
    }*/
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::class,
        ];
        return $behaviors;
    }

    public function beforeAction($action) 	
    {     	
    if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {         	
        Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET PUT');         	
        Yii::$app->end();     	
    }        	     	
    return parent::beforeAction($action); 	
    }


}
